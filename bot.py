from typing import List

import telebot

from telebot.types import Message

from api import crud
from settings import TELEGRAM_TOKEN
from api.utils import get_db


def listener(messages: List[Message]):
    for m in messages:
        chat_id = m.chat.id
        if m.content_type == 'text':
            author_name = '{first_name} {last_name}'.format(
                first_name=m.from_user.first_name,
                last_name=m.from_user.last_name,
            )
            for db in get_db():
                crud.add_message(
                    db=db,
                    message_id=m.message_id,
                    message=m.text,
                    author_name=author_name,
                    author_username=m.from_user.username,
                    chat_id=chat_id,
                    created=m.date,
                )


tb = telebot.TeleBot(TELEGRAM_TOKEN)
tb.set_update_listener(listener)

tb.polling(none_stop=False, interval=5)
