from typing import List

from pydantic.main import BaseModel


class Message(BaseModel):
    id: int
    message_id: int
    message: str
    author_name: str
    author_username: str
    created: int

    class Config:
        orm_mode = True


class Project(BaseModel):
    id: int
    project_id: int
    chat_id: int
    messages: List[Message]

    class Config:
        orm_mode = True
