from sqlalchemy.orm import Session

from api import models


def get_project(db: Session, project_id: int):
    return db.query(models.Project).filter(
        models.Project.project_id == project_id
    ).first()


def get_project_by_chat_id(db: Session, chat_id: int):
    return db.query(models.Project).filter(
        models.Project.chat_id == chat_id
    ).first()


def register_project(db: Session, project_id: int, chat_id: int):
    project = get_project(db, project_id)
    if not project:
        project = models.Project(project_id=project_id, chat_id=chat_id)
    else:
        project.chat_id = chat_id
    db.add(project)
    db.commit()
    db.refresh(project)
    return project


def add_message(db: Session, chat_id: int, message_id: int, message: str,
                author_name: str, author_username: str, created: int):
    project = get_project_by_chat_id(db, chat_id=chat_id)
    if project:
        message = models.Message(
            project_id=project.id, message_id=message_id, message=message,
            author_name=author_name, author_username=author_username,
            created=created
        )
        db.add(message)
        db.commit()
