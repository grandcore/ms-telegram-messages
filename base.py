from fastapi import FastAPI, Depends
from sqlalchemy.orm import Session
from dotenv import load_dotenv

from api import crud, models, schemas
from api.database import engine
from api.utils import get_db

load_dotenv(verbose=True)


models.Base.metadata.create_all(bind=engine)
app = FastAPI()


@app.get("/register", response_model=schemas.Project)
def register_project(project_id: int, chat_id: int,
                     db: Session = Depends(get_db)):
    return crud.register_project(db=db, project_id=project_id, chat_id=chat_id)


@app.get("/get_messages", response_model=schemas.Project)
def register_project(project_id: int, db: Session = Depends(get_db)):
    return crud.get_project(db=db, project_id=project_id)
